package mappe2.factory;

import mappe2.Sjokolade.*;

/**
 * Klasse som produserer sjokoladeplater av ulike størrelser på kommando.
 */
public class BreiaFabrikk {
    public Sjokoladeplate lagSjokoladePlate(String valg){
        switch (valg){
            case "v": return new SjokoladeVanlig();
            case "l": return new SjokoladeLiten();
            default: return null;
        }
    }
}

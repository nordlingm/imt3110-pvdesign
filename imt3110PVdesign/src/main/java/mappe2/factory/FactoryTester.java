package mappe2.factory;

import mappe2.Sjokolade.Sjokoladeplate;

import java.util.Scanner;

/**
 * Oppretter og bruker et eksempel av factory method
 */
public class FactoryTester {            // Viser factory method (FM)
    public static void main(String[] args) {
        // Oppsett av klasser, og bestemme hva som skal lages
        BreiaFabrikk breiaFabrikk = new BreiaFabrikk();
        Sjokoladeplate sjokoPlate;
        Scanner scanner = new Scanner(System.in);
        String valg;
        System.out.printf("1: %s%n2: %s%nVelg Sjokolade å kjøpe(l - v): ", "Vanlig", "Liten");
        // bruk av FM inne i if'en
        if (scanner.hasNextLine()){
            valg = scanner.nextLine();
            sjokoPlate = breiaFabrikk.lagSjokoladePlate(valg);

            System.out.println("Du kjøpte en " + sjokoPlate.toString());
        } else{
            System.out.println("Du valgte å ikke kjøpe en sjokolade");
        }
    }
}

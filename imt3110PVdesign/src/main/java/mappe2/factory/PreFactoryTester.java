package mappe2.factory;

import mappe2.Sjokolade.*;

import java.util.Scanner;

public class PreFactoryTester {
    public static void main(String[] args) {
        // Oppsett av klasser og bestemme
        BreiaFabrikk breiaFabrikk = new BreiaFabrikk(); // Opp
        Sjokoladeplate sjokoPlate = null;
        Scanner scanner = new Scanner(System.in);
        String valg;
        System.out.printf("1: %s%n2: %s%nVelg Sjokolade å kjøpe(l - v): ", "Vanlig", "Liten");
        // Valg av sjokoladeplate før bruken av
        if (scanner.hasNextLine()){
            valg = scanner.nextLine();
            switch (valg){
                case "v": sjokoPlate = new SjokoladeVanlig(); break;
                case "l": sjokoPlate = new SjokoladeLiten();  break;
            }
            System.out.println("Du kjøpte en " + sjokoPlate.toString());
        } else{
            System.out.println("Du valgte å ikke kjøpe en sjokolade");
        }
    }
}

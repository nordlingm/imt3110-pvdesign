package mappe2.visitor;

import mappe2.Godteri.Godteri;
import mappe2.Sjokolade.Sjokoladeplate;

/**
 * Beregner dekningsbidrag av produkter.
 */
public class BeregnDB implements Visitor {
    /**
     * Beregner dekningsbidrag for klasser i sjokoladeplatehierarkiet.
     * @param sjokoladeplate Sjokoladeplate - den abstrakte klassen for sjokolade.
     * @return double - den beregnede dekningsbidraget på en sjokoladeplate
     */
    @Override
    public double visit(Sjokoladeplate sjokoladeplate) {  // den besøkte som parameter
        return sjokoladeplate.getPris() - sjokoladeplate.getProdKostnad();
    }

    /**
     * Beregner dekningsbidrag for Godteriklassen..
     * @param godteri Godteri - klassen som det skal beregnes DB for.
     * @return double - den beregnede dekningsbidraget.
     */
    @Override
    public double visit(Godteri godteri) {
        return godteri.getPris() - godteri.getKostnad();
    }
}

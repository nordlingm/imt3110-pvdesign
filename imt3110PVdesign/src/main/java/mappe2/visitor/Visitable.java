package mappe2.visitor;

/**
 * Interface for de klassene som skal bli besøkt.
 */
public interface Visitable {
    public double accept(Visitor visitor);
}

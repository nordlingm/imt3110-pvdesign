package mappe2.visitor;

import mappe2.Godteri.*;
import mappe2.Sjokolade.*;

/**
 * Interface for de klassene som skal besøke andre klasser.
 */
public interface Visitor {
    public double visit(Sjokoladeplate sjokoladeplate);
    public double visit(Godteri godteri);
}

package mappe2.visitor;
import mappe2.Sjokolade.SjokoladeLiten;
import mappe2.Sjokolade.SjokoladeVanlig;
import mappe2.Sjokolade.Sjokoladeplate;
import mappe2.Godteri.Godteri;

public class VisitorTester {
    public static void main(String[] args) {
        // Visitee-klassene
        Sjokoladeplate sjokoladeplate1 = new SjokoladeVanlig();
        Sjokoladeplate sjokoladeplate2 = new SjokoladeLiten();
        Godteri godteri = new Godteri();
        // Visitor-klassene
        BeregnDB beregnDB = new BeregnDB();
        BeregnMVA beregnMVA = new BeregnMVA();

        // Output MVA
        System.out.printf("Pris med MVA:%n");
        System.out.printf("Vare %d: %s; før: %.2f, etter: %.2f%n",
                1, sjokoladeplate1.getNavn(),
                sjokoladeplate1.getPris(), sjokoladeplate1.accept(beregnMVA));
        System.out.printf("Vare %d: %s; før: %.2f, etter: %.2f%n",
                2, sjokoladeplate2.getNavn(),
                sjokoladeplate2.getPris(), sjokoladeplate2.accept(beregnMVA));
        System.out.printf("Vare %d: %s; før: %.2f, etter: %.2f%n",
                3, godteri.getNavn(), godteri.getPris(), godteri.accept(beregnMVA));

        // Output dekningsbidrag
        System.out.printf("Dekningsbidrag:%n");
        System.out.printf("Vare %d: %s; Salgspris: %.2f, Kostnad: %.2f; DB: %.2f%n",
                1, sjokoladeplate1.getNavn(), sjokoladeplate1.getPris(),
                sjokoladeplate1.getProdKostnad(), sjokoladeplate1.accept(beregnDB));
        System.out.printf("Vare %d: %s; Salgspris: %.2f, Kostnad: %.2f; DB: %.2f%n",
                2, sjokoladeplate2.getNavn(), sjokoladeplate2.getPris(),
                sjokoladeplate2.getProdKostnad(), sjokoladeplate2.accept(beregnDB));
        System.out.printf("Vare %d: %s; Salgspris: %.2f, Kostnad: %.2f; DB: %.2f%n",
                3, godteri.getNavn(), godteri.getPris(),
                godteri.getKostnad(), godteri.accept(beregnDB));
    }
}

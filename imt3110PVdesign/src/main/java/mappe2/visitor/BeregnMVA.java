package mappe2.visitor;

import mappe2.Godteri.Godteri;
import mappe2.Sjokolade.Sjokoladeplate;

/**
 * Beregner MVA av produkter.
 */
public class BeregnMVA implements Visitor {
    @Override
    public double visit(Sjokoladeplate sjokoladeplate) {
        return sjokoladeplate.getPris() * 1.25;
    }

    @Override
    public double visit(Godteri godteri) {
        return godteri.getPris() * 1.50;
    }
}

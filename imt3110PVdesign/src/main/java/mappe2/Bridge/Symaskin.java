package mappe2.Bridge;

/**
 * Symaskin tilsvarer funksjonen sy i Jakke og Skjorte før bridge-pattern.
 */
public class Symaskin implements Maskin {
    @Override
    public Materiale arbeid(Materiale materiale) {
        materiale.setTilstand(Tilstand.ferdig);
        materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 1.2);
        System.out.println("Matriale tilstand: " + materiale.getTilstand()
                + "; Ny kostnad: " + materiale.getKostnadTilNå());
        return materiale;
    }
}

package mappe2.Bridge;

/**
 * Tilstandene til et Materiale i produksjon.
 */
public enum Tilstand {
    rå, oppkuttet, ferdig, ødelagt
}

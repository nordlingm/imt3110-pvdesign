package mappe2.Bridge.før;

import mappe2.Bridge.Maskin;
import mappe2.Bridge.Materiale;

/**
 * Abstrakt klasse for klær før introduksjon av bridge-pattern.
 * Innmaten i sy og skjær defineres i subklassene. I disse subklassene så er valgene låst.
 */
abstract class KlærFør {
    protected Materiale materiale;

    public KlærFør(Materiale matriale) {
        this.materiale = matriale;
    }

    abstract public void sy();
    abstract public void skjær();
}

package mappe2.Bridge.før;
import mappe2.Bridge.Materiale;
import mappe2.Bridge.Tilstand;

/**
 * Jakke før introduksjon av bridge-pattern.
 */
public class JakkeFør extends KlærFør {
    public JakkeFør(Materiale matriale) {
        super(matriale);
    }
    /**
     * Funksjon som representerer det å sy sammen delene av en jakke.
     */
    @Override
    public void sy() {
        materiale.setTilstand(Tilstand.oppkuttet);
        materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 1.2);
        System.out.println("Matriale tilstand: " + materiale.getTilstand()+ "; Ny kostnad: " + materiale.getKostnadTilNå());

    }
    /**
     * Funksjon som representerer det å skjære til delene av en jakke.
     */
    @Override
    public void skjær() {
        int random = (int)(Math.random()*5) + 1;
        System.out.println(random);
        if (random > 1){
            materiale.setTilstand(Tilstand.ferdig);
            materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 1.5);
        }  else {
            materiale.setTilstand(Tilstand.ødelagt);
            materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 0);
        }
        System.out.println("Matriale tilstand: " + materiale.getTilstand()+ "; Ny kostnad: " + materiale.getKostnadTilNå());
    }
}

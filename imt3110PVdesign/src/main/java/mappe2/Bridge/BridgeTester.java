package mappe2.Bridge;

/**
 * Klasse som tester og lager et output bridge-patternet.
 */
public class BridgeTester {
    public static void main(String[] args) {
        // Kan
        Materiale materiale = new Materiale("Denim", 100.00);
        Materiale materiale2 = new Materiale("Silke", 200);

        // Oppretter en symaskin og 2 skjæremaskiner.
        Maskin symaskin = new Symaskin();
        Maskin skjæreMaskin = new SkjæreMaskin();   // Tildigere skjærefunksjonen i Skjorte
        Maskin skjæremaskin2 = new SkjæreMaskin2(); // Tidligere skjærefunksjon i Jakke

        // Lager to skjorter som bruker hver sin type skjæremaskin.
        // Dette var ikke mulig før introduksjonen av bridge-pattern.
        Klær skjorte = new Skjorte(symaskin, skjæreMaskin, materiale);
        Klær skjorte2 = new Skjorte(symaskin, skjæremaskin2, materiale2);
        skjorte.produser();
        System.out.println(); // kun for å få mellomrom mellom output'ene.
        skjorte2.produser();
    }
}

package mappe2.Bridge;

/**
 * Skjæreklasse for det som tidligere var skjærefunksjonen i Skjorte.
 */
public class SkjæreMaskin implements Maskin {   // Normalt ville dette være i kutte()-funksjonen
                                                //   i både Jakke, Skjorte og alle andre subklasser.
    @Override
    public Materiale arbeid(Materiale materiale) {
        materiale.setTilstand(Tilstand.oppkuttet);      // endrer matrialets tilstand
        materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 1.2);
        System.out.println("Matriale tilstand: " + materiale.getTilstand()
                + "; Ny kostnad: " + materiale.getKostnadTilNå());
        return materiale;
    }
}

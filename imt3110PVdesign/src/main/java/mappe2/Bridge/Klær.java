package mappe2.Bridge;

/**
 * Abstrakt klasse for klær etter introduksjon av bridge-pattern.
 * Forskjellene er at sy og skjær-funksjonene er blitt byttet ut med produserfunksjonen,
 *   og introduksjonen av de to Maskin-klassene.
 * Disse maskinene kan velges etter behov slik at en ikke er låst som i KlærFør-hierarkiet.
 */
abstract class Klær {
    protected Maskin maskin1;
    protected Maskin maskin2;
    protected Materiale materiale;

    public Klær(Maskin maskin1, Maskin maskin2, Materiale matriale) {
        this.maskin1 = maskin1;
        this.maskin2 = maskin2;
        this.materiale = matriale;
    }

    abstract public void produser();
}

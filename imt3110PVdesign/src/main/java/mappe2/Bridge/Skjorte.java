package mappe2.Bridge;

/**
 * En klasse som repreenterer det å beregne kostnadene for en Skjorte
 */
public class Skjorte extends Klær {
    public Skjorte(Maskin maskin1, Maskin maskin2, Materiale materiale) {
        super(maskin1, maskin2, materiale);
    }

    /**
     * Sy og skjær, funksjonene er nå byttet ut med en produserfunksjonen som representerer produksjon.
     */
    @Override
    public void produser() {
        System.out.println("Skjorte: " + materiale.getMatrialType() + "; Tilstand: "
                + materiale.getTilstand() +"; kostnad til nå: " + materiale.getKostnadTilNå());
        if (materiale.getTilstand() == Tilstand.rå){
            maskin2.arbeid(materiale);  // En bruker den første maskinen
        }                               // Normalt ville en intern funksjon vært her stedenfor
        if (materiale.getTilstand() == Tilstand.oppkuttet){
            maskin1.arbeid(materiale);  // En bruker den andre maskinen
        } else {
            System.out.println("Kan ikke sende oppkuttet til symaskin");
        }
    }
}

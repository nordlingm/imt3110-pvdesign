package mappe2.Bridge;

/**
 * Skjæreklasse for det som tidligere var skjærefunksjonen i Jakke.
 */
public class SkjæreMaskin2 implements Maskin {

    @Override
    public Materiale arbeid(Materiale materiale) {
        int random = (int)(Math.random()*5) + 1;
        if (random > 1){
            materiale.setTilstand(Tilstand.oppkuttet);      // endrer materialets tilstand
            materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 1.2);
            System.out.println("Matriale tilstand: " + materiale.getTilstand()
                    + "; Ny kostnad: " + materiale.getKostnadTilNå());
        } else {
            materiale.setTilstand(Tilstand.ødelagt);
            materiale.setKostnadTilNå(materiale.getKostnadTilNå() * 0);
            System.out.println("Matriale tilstand: " + materiale.getTilstand()
                    + "; Ny kostnad: " + materiale.getKostnadTilNå());
        }
        return materiale;
    }
}

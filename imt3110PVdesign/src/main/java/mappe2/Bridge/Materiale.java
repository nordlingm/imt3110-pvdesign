package mappe2.Bridge;
import static mappe2.Bridge.Tilstand.rå;

/**
 * Hvilket materiale en Skjorte eller jakke er laget av.
 */
public class Materiale {
    private String matrialType;
    private double kostnadTilNå;
    private Tilstand tilstand;

    public Materiale(String matrialType, double kostnadTilNå) {
        this.matrialType = matrialType;
        this.kostnadTilNå = kostnadTilNå;
        this.tilstand = rå;
    }

    // set- og get-funksjoner for Materialer
    public String getMatrialType() {
        return matrialType;
    }

    public double getKostnadTilNå() {
        return kostnadTilNå;
    }

    public Tilstand getTilstand() {
        return tilstand;
    }

    public void setKostnadTilNå(double kostnadTilNå) {
        this.kostnadTilNå = kostnadTilNå;
    }

    public void setTilstand(Tilstand tilstand) {
        this.tilstand = tilstand;
    }
}

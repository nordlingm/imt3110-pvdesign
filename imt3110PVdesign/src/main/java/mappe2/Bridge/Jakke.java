package mappe2.Bridge;


public class Jakke extends Klær {
    public Jakke(Maskin maskin1, Maskin maskin2, Materiale matriale) {
        super(maskin1, maskin2, matriale);
    }

    @Override
    public void produser() {
        System.out.println("Jakke: " + materiale.getMatrialType() + "; Tilstand: "
                + materiale.getTilstand() +"; kostnad til nå: " + materiale.getKostnadTilNå());
        if (materiale.getTilstand() == Tilstand.rå){
            maskin2.arbeid(materiale);  // En bruker den første maskinen
        }                               // Normalt ville en intern funksjon vært her stedenfor
        if (materiale.getTilstand() == Tilstand.oppkuttet){
            maskin1.arbeid(materiale);  // En bruker den andre maskinen
        } else {
            System.out.println("Kan ikke sende oppkuttet til symaskin");
        }
    }
}

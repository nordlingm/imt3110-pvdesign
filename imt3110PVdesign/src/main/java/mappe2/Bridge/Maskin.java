package mappe2.Bridge;

/**
 * Interface for de forskjellige klassene.
 * Arbeid er en funksjon med innmat som tilsvarer den som var i sy- og
 *   skjær-funksjonene i Jakke og Skjorte før Bridge-pattern.
 */
public interface Maskin {
    public Materiale arbeid(Materiale materiale);
}

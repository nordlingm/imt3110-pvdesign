package mappe2.Godteri;
import mappe2.visitor.*;

/**
 * Klasse som skal besækes for å vise at visitor-patternet ikke er låst til kun et klasse hierariki
 */
public class Godteri implements Visitable {
    private String navn;
    private double pris;
    private double kostnad;

    public Godteri() {
        navn = "Løsvekt";
        pris = 0.75;
        kostnad = 0.33;
    }

    @Override
    public double accept(Visitor visitor) { // Hvilken visitor skal komme på besøk
        return visitor.visit(this); // Det spesielle ved visit-funksjonen er hvordan
    }                                       //  elementet sender seg selv til visitor.
    public String getNavn() {
        return navn;
    }
    public double getKostnad() {
        return kostnad;
    }
    public double getPris() {
        return pris;
    }

}

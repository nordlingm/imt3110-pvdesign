package mappe2.Sjokolade;
import mappe2.visitor.Visitable;
import mappe2.visitor.Visitor;

public abstract class Sjokoladeplate implements Visitable {
    private String navn;
    private double pris;
    private double prodKostnad;

    public String getNavn() {
        return navn;
    }

    public double getPris() {
        return pris;
    }

    public double getProdKostnad() {
        return prodKostnad;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public void setPris(double pris) {
        this.pris = pris;
    }

    public void setProdKostnad(double prodKostnad) {
        this.prodKostnad = prodKostnad;
    }

    @Override
    public String toString() {
        return getNavn() + " til kr " + getPris();
    }

    @Override
    public double accept(Visitor visitor) {
        return visitor.visit(this);
    }
}

package mappe2.Sjokolade;

public class SjokoladeVanlig extends Sjokoladeplate {
    public SjokoladeVanlig() {
        setNavn("Sjokoladeplate (vanlig)");
        setPris(39.50);
        setProdKostnad(12.34);
    }
}

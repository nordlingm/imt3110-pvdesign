package mappe2.Sjokolade;

public class SjokoladeLiten extends Sjokoladeplate {
    public SjokoladeLiten() {
        setNavn("Sjokoladeplate (liten)");
        setPris(19.50);
        setProdKostnad(5.87);
    }
}
